/****************************************************
 * Description: DAO for t_sso_project
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-05-09 zhanghejie Create File
**************************************************/
package com.xjj.sso.project.dao;

import com.xjj.sso.project.entity.ProjectEntity;
import com.xjj.framework.dao.XjjDAO;

public interface ProjectDao  extends XjjDAO<ProjectEntity> {
	
}

