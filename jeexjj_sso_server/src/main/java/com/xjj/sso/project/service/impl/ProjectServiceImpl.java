/****************************************************
 * Description: ServiceImpl for t_sso_project
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-05-09 zhanghejie Create File
**************************************************/

package com.xjj.sso.project.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xjj.framework.dao.XjjDAO;
import com.xjj.framework.service.XjjServiceSupport;
import com.xjj.framework.utils.StringUtils;
import com.xjj.framework.web.support.XJJParameter;
import com.xjj.sso.project.dao.ProjectDao;
import com.xjj.sso.project.entity.ProjectEntity;
import com.xjj.sso.project.service.ProjectService;

@Service
public class ProjectServiceImpl extends XjjServiceSupport<ProjectEntity> implements ProjectService {

	@Autowired
	private ProjectDao projectDao;

	@Override
	public XjjDAO<ProjectEntity> getDao() {
		
		return projectDao;
	}
	
	public ProjectEntity getByCode(String code)
	{
		if(StringUtils.isBlank(code))
		{
			return null;
		}
		XJJParameter query = new XJJParameter();
		query.addQuery("query.code@eq@s", code);
		
		List<ProjectEntity> projectList = projectDao.findList(query.getQueryMap());
		if(null==projectList ||projectList.size()==0)
		{
			return null;
		}
		return projectList.get(0);
	}
}