package com.xjj.sso.server.ticket;


public class ServiceTicket extends Ticket{
	private static final long serialVersionUID = 1L;
	
	private String service;
	private String host;
	// 来自的项目
	private String projectCode;
	
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}
	
	public String getProjectCode() {
		return projectCode;
	}
	public void setProjectCode(String projectCode) {
		this.projectCode = projectCode;
	}
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		sb.append("id="+this.getId());
		sb.append("&service="+this.getService());
		sb.append("&host="+this.getHost());
		sb.append("&expired="+this.isExpired());
		sb.append("]");
		return sb.toString();
	}
}