package com.xjj.sso.server.util;

import java.util.Properties;

public interface SSOConfigLoader{
	/**
	 * Load Config
	 * @param name fileName or Database
	 */
	public Properties load() throws Exception;
	
	public void save(Properties properties) throws Exception;
}
