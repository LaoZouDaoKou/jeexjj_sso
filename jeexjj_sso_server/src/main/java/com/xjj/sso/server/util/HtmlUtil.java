package com.xjj.sso.server.util;

import com.xjj.framework.utils.StringUtils;

public class HtmlUtil {

	private static String[] htmlArr = new String[]{"<p>","</p>"};
	private static String[] validateHtmlArr = new String[]{"<p>","</p>","<br/>","&nbsp;"};
	private static String nullStr = "";
	
	/**
	 * 过滤html标签
	 * @param str
	 * @return
	 */
	public static String filterHtml(String str)
	{
		if(null == str)
		{
			return nullStr;
		}
		
		for (int i = 0; i < htmlArr.length; i++) {
			str = str.replaceAll(htmlArr[i],nullStr);
		}
		return str;
	}
	
	
	
	/**
	 * 过滤html标签
	 * @param str
	 * @return
	 */
	public static boolean isBlankHtml(String str)
	{
		if(null == str)
		{
			return true;
		}
		
		for (int i = 0; i < validateHtmlArr.length; i++) {
			str = str.replaceAll(validateHtmlArr[i],nullStr);
		}
		
		if(StringUtils.isBlank(str))
		{
			return true;
		}else
		{
			return false;
		}
	}
}
